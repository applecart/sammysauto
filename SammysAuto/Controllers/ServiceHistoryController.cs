﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SammysAuto.Data;

namespace SammysAuto.Controllers
{
    public class ServiceHistoryController : Controller
    {
        public async Task<IActionResult> Index(int id)
        {
            ViewBag.PreviousAction = "Index";
            ViewBag.PreviousActionParameters = new { Id = GetCustomerId(id) };
            ViewBag.PreviousController = "Cars";
            ViewData["Title"] = string.Format("Service History of {0}'s {1}", GetCustomerName(id), GetCarname(id));
            ViewBag.CrudAction = "Add";
            ViewBag.CrudActionParameters = new { carId = id };
            var serviceHistory = await _db.ServiceHistory.Where(m => m.CarId == id).ToListAsync();

            return View(serviceHistory);

        }

        public IActionResult Add(int carId)
        {
            PrepFormControls(new { id = carId });
            ViewData["Title"] = "Adding Service History";
            ViewBag.CrudAction = "Create";
            var vm = new SammysAuto.Models.ServiceHistory();
            vm.CarId = carId;
            vm.ServiceDt = DateTime.Now;
            return View("Edit", vm);

        }

        public async Task<IActionResult> Edit(int serviceId, int carId)
        {
            PrepFormControls(new { id = carId });
            ViewData["Title"] = "Editing a Service Record";
            ViewBag.CrudAction = "Update";

            return View(await _db.ServiceHistory.FirstAsync(m => m.Id == serviceId));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SammysAuto.Models.ServiceHistory  serviceHistory)
        {
            ViewData["Title"] = "Adding Service History";
            if (!ModelState.IsValid) {
                PrepFormControls(new { id = serviceHistory.CarId });
                return View("Edit", serviceHistory);
                }

            _db.Add(serviceHistory);
            await _db.SaveChangesAsync();

            return RedirectToAction(GetIndexAction(serviceHistory.CarId.ToString()));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(SammysAuto.Models.ServiceHistory serviceHistory, string updateType)
        {
            ViewData["Title"] = "Editing a Service Record";
            if (updateType.Equals("Delete"))
            {
                _db.Remove(serviceHistory);

            }
            else
            {
                if (!ModelState.IsValid)
                {
                    PrepFormControls(new { id = serviceHistory.CarId });
                    return View("Edit", serviceHistory);
                }
                _db.Update(serviceHistory);

            }
            await _db.SaveChangesAsync();
            return RedirectToAction(GetIndexAction(serviceHistory.CarId.ToString()));

        }

        private string GetIndexAction(string id = null)
        {
            return string.Format("{0}{1}", nameof(Index), id == null ? string.Empty : @"/" + id);
        }

        private void PrepFormControls(object previousActionParameters = null)
        {
            ViewBag.PreviousActionParameters = previousActionParameters; 
            ViewBag.ServiceTypes = _db.ServiceTypes.ToList().OrderBy(m => m.Name);

        }

        private string GetCustomerId(int carId)
        {
            var car = _db.Cars.First(m => m.Id == carId);
            return car.CustomerId; 
        }

        private string GetCarname(int carId)
        {
            var car = _db.Cars.First(m => m.Id == carId);
            return string.Format("{0} {1}",car.Make, car.Model) ;
        }

        private string GetCustomerName(int carId)
        {
            var customer = _db.Users.First(m => m.Id == GetCustomerId(carId));
            return customer.Name;

        }

        public ServiceHistoryController(ApplicationDbContext db) { _db = db; }
        private readonly ApplicationDbContext _db;
        protected override void Dispose(bool disposing) { _db.Dispose(); }


    } // class
}