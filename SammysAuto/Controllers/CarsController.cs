﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SammysAuto.Data;

namespace SammysAuto.Controllers
{
    public class CarsController : Controller
    {
        private readonly ApplicationDbContext _db;
        public CarsController(ApplicationDbContext db) { _db = db; }
        protected override void Dispose(bool disposing) { _db.Dispose(); }

        public IActionResult Index(string id)
        {
            ViewBag.PreviousAction = "Index";
            ViewBag.PreviousController = "Customers";
            ViewData["Title"] = string.Format("{0}'s cars", GetCustomerName(id));
            ViewBag.CrudAction = "Add";
            ViewBag.CrudActionParameters = new { customerId = id }; 
            var cars = _db.Cars.Where(m => m.CustomerId == id).ToList();
            return View(cars);

        }

        public IActionResult Add(string customerId)
        {
            PrepFormControls(new { id = customerId});
            ViewData["Title"] = string.Format("{0}: Adding car", GetCustomerName(customerId));
            ViewBag.CrudAction = "Create";
            var car = new SammysAuto.Models.Car();
            car.CustomerId = customerId; 
            return View("Edit", car);

        }

        public IActionResult Edit(int id)
        {
            var row = _db.Cars.First(m => m.Id == id);
            PrepFormControls(new { id = row.CustomerId });
            ViewData["Title"] = string.Format("{0}: Editing car", GetCustomerName(row.CustomerId));
            ViewBag.CrudAction = "Update";
            return View(row);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SammysAuto.Models.Car  car)
        {
            ViewData["Title"] = string.Format("{0}: Adding car", GetCustomerName(car.CustomerId));
            if (!ModelState.IsValid) { PrepFormControls(new { id = car.CustomerId } ); return View("Edit", car); }
            _db.Add(car);
            await _db.SaveChangesAsync();
            return RedirectToAction(GetIndexAction(car.CustomerId));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(SammysAuto.Models.Car car, string updateType)
        {
            ViewData["Title"] = string.Format("{0}: Editing car", GetCustomerName(car.CustomerId)); ;
            if (updateType.Equals("Delete"))
            {
                _db.Remove(car);
            }
            else
            {
                if (!ModelState.IsValid) { PrepFormControls(new { id = car.CustomerId }); return View("Edit", car); }
                _db.Update(car);
            }
            await _db.SaveChangesAsync();
            return RedirectToAction(GetIndexAction(car.CustomerId));

        }

        private void PrepFormControls(object previousActionParameters = null)
        {
            ViewBag.PreviousActionParameters = previousActionParameters;
            ViewBag.Makes = _db.CarMakes.ToList().OrderBy(m => m.Make);

        }

        private string GetIndexAction(string id = null)
        {
            return string.Format("{0}{1}", nameof(Index), id == null ? string.Empty : @"/" + id);
        }

        private string GetCustomerName(string customerId)
        {
            return _db.Users.First(m => m.Id == customerId).Name;  
        }


    }

}