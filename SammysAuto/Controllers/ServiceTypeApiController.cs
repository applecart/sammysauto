﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SammysAuto.Data;
using SammysAuto.Models;


namespace SammysAuto.Controllers
{

    [Produces("application/json")]
    [Route("api/ServiceTypeApi")]
    public class ServiceTypeApiController : Controller
    {
        [HttpGet]
        public IActionResult Get()
        {
            var serviceTypes = _DbContext.ServiceTypes;
            return Ok(serviceTypes);

        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var serviceTypes = _DbContext.ServiceTypes;
            var serviceType = serviceTypes.Find(id); // find by primary key

            return Ok(serviceType);

        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ServiceType serviceType)
        {
            var serviceTypes = _DbContext.ServiceTypes;
            serviceTypes.Add(serviceType);
            await _DbContext.SaveChangesAsync();
            return Ok();

        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] ServiceType serviceType)
        {
            var serviceTypes = _DbContext.ServiceTypes;
            serviceTypes.Update(serviceType);
            await _DbContext.SaveChangesAsync();
            return Ok();

        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] ServiceType serviceType)
        {
            var serviceTypes = _DbContext.ServiceTypes;
            serviceTypes.Remove(serviceType);
            await _DbContext.SaveChangesAsync();
            return Ok();

        }

        private readonly ApplicationDbContext _DbContext;
        public ServiceTypeApiController(ApplicationDbContext db) { _DbContext = db; } // ctor
        protected override void Dispose(bool disposing) { _DbContext.Dispose(); }


    } // class

} // namespace
