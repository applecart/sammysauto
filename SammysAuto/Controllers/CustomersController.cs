﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SammysAuto.Data;

namespace SammysAuto.Controllers
{
    public class CustomersController : Controller
    {
        private readonly ApplicationDbContext _db;
        public CustomersController(ApplicationDbContext db) { _db = db; }
        protected override void Dispose(bool disposing) { _db.Dispose(); }

        public IActionResult Index()
        {
            ViewData["Title"] = "List of customers";
            ViewBag.AddController = "Account";
            ViewBag.CrudAction = "Register";
            var customers = _db.Users.ToList();  
            return View(customers);

        }

    }
}