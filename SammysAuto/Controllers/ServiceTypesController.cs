﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SammysAuto.Data;
using SammysAuto.Models;

namespace SammysAuto.Controllers
{
    public class ServiceTypesController : Controller
    {

        public IActionResult Index()
        {
            ViewData["Title"] = "List of available services";
            ViewBag.CrudAction = "Add";

            var serviceTypes = _DbContext.ServiceTypes;

            //var webApi = new WebApiWrapper<ServiceType>(CommonCode.UrlCombine(CommonCode.GetAppUrl(HttpContext), "api", "ServiceTypeApi"));
            //var serviceTypes = webApi.Get();

            return View(serviceTypes);

        }

        public IActionResult Add()
        {
            ViewData["Title"] = "Adding a Service Type";
            ViewBag.CrudAction = "Create"; 

            return View("Edit", new ServiceType());

        }

        public IActionResult Edit(int id)
        {
            ViewData["Title"] = "Editing a Service Type";
            ViewBag.CrudAction = "Update";

            var serviceTypes = _DbContext.ServiceTypes;
            var serviceType = serviceTypes.Find(id); // find by primary key

            //var webApi = new WebApiWrapper<ServiceType>(CommonCode.UrlCombine(CommonCode.GetAppUrl(HttpContext), "api", "ServiceTypeApi"));
            //var serviceType = webApi.Get(id.ToString());

            return View(serviceType);

        }

        [HttpPost][ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ServiceType serviceType)
        //public IActionResult Create(ServiceType serviceType)
        {
            ViewData["Title"] = "Adding a Service Type";
            if (!ModelState.IsValid) return View("Edit",serviceType);

            var serviceTypes = _DbContext.ServiceTypes;
            await serviceTypes.AddAsync(serviceType);
            await _DbContext.SaveChangesAsync();

            //var webApi = new WebApiWrapper<ServiceType>(CommonCode.UrlCombine(CommonCode.GetAppUrl(HttpContext), "api", "ServiceTypeApi"));
            //webApi.Post(serviceType);

            return RedirectToAction(GetIndexAction());

        }

        [HttpPost] [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(ServiceType serviceType, string updateType)
        //public IActionResult Update(ServiceType serviceType, string updateType)
        {
            ViewData["Title"] = "Editing a Service Type";

            var serviceTypes = _DbContext.ServiceTypes;
            //var webApi = new WebApiWrapper<ServiceType>(CommonCode.UrlCombine(CommonCode.GetAppUrl(HttpContext), "api", "ServiceTypeApi"));

            if (updateType.Equals("Delete"))
            {
                serviceTypes.Remove(serviceType);
                await _DbContext.SaveChangesAsync();

                //webApi.Delete(serviceType);

            }
            else
            {
                if (!ModelState.IsValid) return View("Edit", serviceType);

                serviceTypes.Update(serviceType);
                await _DbContext.SaveChangesAsync();

                //webApi.Put(serviceType);

            }
            return RedirectToAction(GetIndexAction());

        }

        private string GetIndexAction(string id = null)
        {
            return string.Format("{0}{1}", nameof(Index), id == null ? string.Empty : @"/" + id);
        }

        private readonly ApplicationDbContext _DbContext;
        public ServiceTypesController(ApplicationDbContext db) { _DbContext = db; } // ctor
        protected override void Dispose(bool disposing) { _DbContext.Dispose(); }

    } // class

} // namespace