﻿using Microsoft.AspNetCore.Identity;
/*
This class helps with defining and creating roles of an app
How to use this class
	Copy this class into folder App_Code as it is a developer defined class
	Define all the roles in the private method called GetRolesOfApp()
	In Startup.cs, change the configure() by adding new param roleManager as follows
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app
            , IHostingEnvironment env
            ,RoleManager<IdentityRole> roleManager
            )

	Modify the line app.UseAuthentication(); as
 		app.UseAuthentication(); appRoles.Create(roleManager); 
*/

public static class appRoles
{

    private static string[] GetRolesOfApp()
    {
        return new string[] { // Add roles of the app below
            "Administrator"
            ,"Standard"
        };

    }

    public static void  Create(RoleManager<IdentityRole> roleManager)
    {
        foreach (var roleName in GetRolesOfApp())
        {
            if (!roleManager.RoleExistsAsync(roleName).Result)
            {
                var role = new IdentityRole();
                role.Name = roleName;
                role.NormalizedName = roleName.ToUpperInvariant();  
                IdentityResult roleResult = roleManager.CreateAsync(role).Result;   
            }
        }
    }

} // class
