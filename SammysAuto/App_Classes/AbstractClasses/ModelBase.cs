﻿namespace AbstractClasses
{
    public abstract class ModelBase
    {
        public abstract int Id { get; set; }
    }
}