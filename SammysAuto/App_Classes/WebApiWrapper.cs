﻿// Class abstracts Web Api interfacing chores

public class WebApiWrapper<T>
{
    private string _url;
    public WebApiWrapper(string url)
    {
        _url = url;

    }

    public System.Collections.Generic.List<T> Get()
    {
        using (var client = new System.Net.WebClient())  
        {
            PrepClient(client);
            client.Headers.Add("Accept:application/json");
            var data = client.DownloadString(GetUrl());
            return Newtonsoft.Json.JsonConvert.DeserializeObject<System.Collections.Generic.List<T>>(data); 
        }

    }

    public T Get(string id)
    {
        using (var client = new System.Net.WebClient())  
        {
            PrepClient(client);
            client.Headers.Add("Accept:application/json");
            var data = client.DownloadString(GetUrl(id));
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(data);
        }

    }

    public void Post(T fromBody) { Dml(fromBody, RestVerb.POST); }
    public void Put(T fromBody) { Dml(fromBody, RestVerb.PUT); }
    public void Delete(T fromBody) { Dml(fromBody, RestVerb.DELETE); }

    private enum RestVerb
    {
        POST
       ,PUT
       ,DELETE
    }

    private void Dml(T fromBody, RestVerb verb)
    {
        using (var client = new System.Net.WebClient())
        {
            PrepClient(client);
            var data = Newtonsoft.Json.JsonConvert.SerializeObject(fromBody);
            client.UploadString(GetUrl(), verb.ToString(), data);
        }

    }

    private void PrepClient(System.Net.WebClient client)
    {
        client.Headers.Add("Content-Type:application/json"); //Content-Type
        
    }

    private string GetUrl()
    {
        return _url;

    }

    private string GetUrl(string id)
    {
        return string.Format(@"{0}/{1}", _url, id);

    }

}
