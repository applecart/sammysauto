﻿

public static class CommonCode
{
    public static string GetAppUrl(Microsoft.AspNetCore.Http.HttpContext context)
    {
        return string.Format(@"{0}://{1}",context.Request.Scheme, context.Request.Host); 
    }

    public static string UrlCombine(params string[] urls)
    {
        string retVal = string.Empty;
        foreach (string url in urls)
        {
            var path = url.Trim().TrimEnd('/').TrimStart('/').Trim();
            retVal = string.IsNullOrWhiteSpace(retVal) ? path : new System.Uri(new System.Uri(retVal + "/"), path).ToString();
        }
        return retVal;

    }
}
