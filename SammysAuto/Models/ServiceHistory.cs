﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SammysAuto.Models
{
    public class ServiceHistory
    {
        public int Id { get; set; }
        public int CarId { get; set; }

        [DataType(DataType.Date)]
        [Required]
        public DateTime ServiceDt { get; set; }

        [StringLength(50)]
        [Required]
        public string ServiceDesc { get; set; }

    }
}
