﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SammysAuto.Models
{
    [Table("tbl_DC_Cars")]
    public class Car
    {
        public int Id { get; set; }

        [StringLength(50)][Required]
        public string CustomerId { get; set; }

        [Required]
        public string VIN { get; set; }

        [StringLength(50)]
        public string Color { get; set; }

        [StringLength(50)]
        public string Make { get; set; }

        [StringLength(50)]
        public string Model { get; set; }
    }

}
