﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SammysAuto.Models
{
    public class ServiceType : AbstractClasses.ModelBase 
    {
        [Display(Name = "Service#")]
        public override int Id { get; set; }

        [StringLength(50)] [Required]
        public string Name { get; set; }

    }
}
