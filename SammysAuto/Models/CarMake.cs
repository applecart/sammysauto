﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SammysAuto.Models
{
    public class CarMake
    {
        public int Id { get; set; }

        [StringLength(50)]
        [Required]
        public string Make { get; set; }
    }
}
